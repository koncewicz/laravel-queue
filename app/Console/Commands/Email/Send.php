<?php

namespace App\Console\Commands\Email;

use Illuminate\Console\Command;
use Swift_Message;

class Send extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transport = new \Swift_SmtpTransport('smtp.gmail.com', 587, 'tls');

        $transport->setUsername('mkkoncewicz@gmail.com');
        $transport->setPassword(env('SMTP_PASSWORD'));

        $mailer = new \Swift_Mailer($transport);

        $message = (new Swift_Message('Here Subject'))
            ->setFrom(['mkkoncewicz@gmail.com' => 'Marek Koncewicz'])
            ->setTo('marek.koncewicz@gfieast.com')
            ->setBody('Example Body');

        $mailer->send($message);
    }
}
